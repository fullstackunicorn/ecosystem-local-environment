# gitlab.com > fullstackunicorn > local_development_environment > backend > Dockerfile

# DEFINE DOCKER IMAGE ---------------------------------------------------
FROM ubuntu:bionic-20220105
# -----------------------------------------------------------------------

# ASSIGN CONTAINER NAME -------------------------------------------------
ENV container backend
# -----------------------------------------------------------------------

# CONFIGURE VAGRANT ON DOCKER -------------------------------------------
RUN echo INSTALL VAGRANT REQUIREMENTS:
RUN apt-get update -y && apt-get dist-upgrade -y
RUN apt-get install -y --no-install-recommends ssh sudo libffi-dev systemd openssh-client
# add vagrant user and key for SSH
RUN echo CONFIGURE VAGRANT:
RUN useradd --create-home -s /bin/bash vagrant
RUN echo -n 'vagrant:vagrant' | chpasswd
RUN echo 'vagrant ALL = NOPASSWD: ALL' > /etc/sudoers.d/vagrant
RUN chmod 440 /etc/sudoers.d/vagrant
RUN mkdir -p /home/vagrant/.ssh
RUN chmod 700 /home/vagrant/.ssh
RUN echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ==" > /home/vagrant/.ssh/authorized_keys
RUN chmod 600 /home/vagrant/.ssh/authorized_keys
RUN chown -R vagrant:vagrant /home/vagrant/.ssh
RUN sed -i -e 's/Defaults.*requiretty/#&/' /etc/sudoers
RUN sed -i -e 's/\(UsePAM \)yes/\1 no/' /etc/ssh/sshd_config
# -----------------------------------------------------------------------

# INSTALL LINUX ESSENTIALS [CURL, NANO, BUILD-ESSENTIALS] ---------------------
RUN echo CHECK AND INSTALL UPDATE:
RUN sudo apt update -y && sudo apt dist-upgrade -y
RUN echo INSTALL CURL:
RUN sudo apt install -y curl
RUN echo INSTALL NANO:
RUN sudo apt install -y nano
RUN echo INSTALL BUILD ESSENTIALS:
RUN sudo apt-get update -y && sudo apt-get install -y build-essential 
# -----------------------------------------------------------------------

# INSTALL NODE ----------------------------------------------------------
RUN echo INSTALL NODE:
RUN curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash - 
RUN sudo apt install -y nodejs 
RUN node -v
# -----------------------------------------------------------------------

# INSTALL YARN ----------------------------------------------------------
RUN echo INSTALL YARN:
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - 
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
RUN sudo apt update -y
RUN sudo apt install -y yarn
RUN yarn --version
# -----------------------------------------------------------------------

# INSTALL GIT -----------------------------------------------------------
RUN echo INSTALL GIT:
RUN sudo apt install -y software-properties-common
RUN sudo add-apt-repository ppa:git-core/ppa -y 
RUN sudo apt-get update -y && sudo apt-get install -y git 
# -----------------------------------------------------------------------

# INSTALL IMAGEMAGIC ----------------------------------------------------
RUN echo INSTALL IMAGEMAGICK:
RUN apt-get install -y apt-utils openssl
RUN apt-get install -y imagemagick
# -----------------------------------------------------------------------

# INSTALL POSTGRES ------------------------------------------------------
# postgres version
ENV PGSQL_VERSION_TO_INSTALL="14"
# -------------------------------
RUN echo INSTALL WGET:
RUN sudo apt install -y wget
RUN echo INSTALL POSTGRES:
RUN sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
RUN sudo apt-get update
ENV TZ=Europe/Madrid
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN sudo apt-get install -y postgresql-$PGSQL_VERSION_TO_INSTALL
# -------------------------------
# switch to the LINUX USER "postgres" (created by postgres package when installed)
USER postgres
# create a new database user
ENV DATABASE_USER="vagrant" 
ENV DATABASE_PASSWORD="vagrant"
RUN /etc/init.d/postgresql start &&\
    psql --command "CREATE USER $DATABASE_USER WITH SUPERUSER PASSWORD '$DATABASE_PASSWORD';" &&\
    # to add a database decomment a line and replace "DatabaseName"
    # -------------------------------
    # createdb -O $DATABASE_USER "DatabaseName" &&\
    # createdb -O $DATABASE_USER "DatabaseName" &&\
    # createdb -O $DATABASE_USER "DatabaseName" &&\
    createdb -O $DATABASE_USER "_DATABASENAME_" 
    # -------------------------------
# configure who can access postgres databases (everyone)
RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/$PGSQL_VERSION_TO_INSTALL/main/pg_hba.conf
# configure the address where postgres can be called (can be any address)
RUN echo "listen_addresses='*'" >> /etc/postgresql/$PGSQL_VERSION_TO_INSTALL/main/postgresql.conf
# expose the PostgreSQL port
EXPOSE 5432
# switch back to the LINUX USER "root"
USER root
# -----------------------------------------------------------------------

# START SSH DAEMON ------------------------------------------------------
RUN mkdir /var/run/sshd
EXPOSE 22
RUN /usr/sbin/sshd
# -----------------------------------------------------------------------

# KEEP CONTAINER ALIVE --------------------------------------------------
# give the command that Docker monitor to keep the container alive
CMD ["/lib/systemd/systemd"]
# -----------------------------------------------------------------------
